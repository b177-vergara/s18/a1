
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Squirtle", "Charmander", "Lugia"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},

	talk: function(){
		console.log("Pikachu! I choose you!");
	}
}

console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method:");
trainer.talk();

function pokemon(name, level){
		
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
		
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		let newHP = (target.health - this.attack);
		console.log(target.name + "'s health is now reduced to " + newHP);
		if(newHP <= 0){
			this.faint(target);
		}
		target.health = newHP;
		console.log(target);
	}
	this.faint = function(target){
		console.log(target.name + " fainted");
	}
}
	
	let pikachu = new pokemon("Pikachu", 12);
	let bulbasaur = new pokemon("Bulbasaur", 8);
	let charmander = new pokemon("Charmander", 8);
	let squirtle = new pokemon("Squirtle", 8);
	let pidgey = new pokemon("Pidgey", 5);
	let zubat = new pokemon("Zubat", 7);
	let weedle = new pokemon("Weedle", 4);
	let articuno = new pokemon("Articuno", 70);
	let moltres = new pokemon("Moltres", 70);
	let zapdos = new pokemon("Zapdos", 70);
	let lugia = new pokemon("Lugia", 95);

	console.log(pikachu);
	console.log(bulbasaur);
	console.log(charmander);
	console.log(squirtle);
	console.log(pidgey);
	console.log(zubat);
	console.log(weedle);
	console.log(articuno);
	console.log(moltres);
	console.log(zapdos);
	console.log(lugia);

	weedle.tackle(zapdos);
	zapdos.tackle(lugia);
	pikachu.tackle(pidgey);